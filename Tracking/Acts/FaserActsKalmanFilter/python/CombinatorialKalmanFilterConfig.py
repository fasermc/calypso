# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS and FASER collaborations

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg


def CombinatorialKalmanFilter_OutputCfg(flags):
    acc = ComponentAccumulator()
    acc.merge(OutputStreamCfg(flags, "ESD"))
    ostream = acc.getEventAlgo("OutputStreamESD")
    ostream.TakeItemsFromInput = True
    return acc

def CombinatorialKalmanFilter_OutputAODCfg(flags):
    acc = ComponentAccumulator()
    itemList = ["xAOD::EventInfo#*",
                "xAOD::EventAuxInfo#*",
                "FaserSCT_RDO_Container#*",
                "xAOD::FaserTriggerData#*",
                "xAOD::FaserTriggerDataAux#*",
                "ScintWaveformContainer#*",
                "TrackCollection#*",
                "xAOD::WaveformHitContainer#*",
                "xAOD::WaveformHitAuxContainer#*",
                "xAOD::WaveformClock#*",
                "xAOD::WaveformClockAuxInfo#*"
                ]
    acc.merge(OutputStreamCfg(flags, "AOD",itemList))
    ostream = acc.getEventAlgo("OutputStreamAOD")
    ostream.TakeItemsFromInput = True
    return acc



def CombinatorialKalmanFilterCfg(flags, **kwargs):
    acc = ComponentAccumulator()
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    combinatorialKalmanFilterAlg = CompFactory.CombinatorialKalmanFilterAlg(**kwargs)
    acc.addEventAlgo(combinatorialKalmanFilterAlg)
    acc.merge(CombinatorialKalmanFilter_OutputCfg(flags))
    acc.merge(CombinatorialKalmanFilter_OutputAODCfg(flags))
    return acc
