#include "xAODFaserTracking/StripRawData.h"

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

namespace xAOD {
  StripRawData::StripRawData() { }

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER(StripRawData, uint64_t, id, setId)

  static const SG::AuxElement::Accessor<uint32_t> word_acc("dataword");
  void StripRawData::setWord(uint32_t new_word) {
    word_acc(*this) = new_word;
  }

  uint32_t StripRawData::getWord() const {
    return word_acc(*this);
  }

  // decode size of group of strips information 
  int StripRawData::getGroupSize() const {
    return getWord() & 0x7FF;
  }

  // decode strip information
  int StripRawData::getStrip() const {
    return (getWord() >> 11) & 0x7FF;
  }

  // decode time bin information
  int StripRawData::getTimeBin() const {
    return (getWord() >> 22) & 0x7;
  }

  // returns a word incoding the errors
  int StripRawData::getErrors() const {
    return (getWord() >> 25) & 0x7;
  }

  // returns true if the time bin corresponding to the present BC is on
  bool StripRawData::OnTime() const {
    return (getWord() >> 23) & 0x1;
  }

  // returns true if there is an error in the first hit's data
  bool StripRawData::FirstHitError() const {
    return (getWord() >> 29) & 0x1;
  }

  // returns true if there is an error in the second hit's data
  bool StripRawData::SecondHitError() const {
    return (getWord() >> 30) & 0x1;
  }
} // namespace xAOD