# Copyright (C) 2020-2021 CERN for the benefit of the FASER collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg


# One stop shopping for normal FASER data
def CaloWaveformDigitizationCfg(flags):
    """ Return all algorithms and tools for Waveform digitization """
    acc = ComponentAccumulator()

    if not flags.Input.isMC:
        return

    acc.merge(CaloWaveformDigiCfg(flags, "CaloWaveformDigiAlg"))
    return acc

# Return configured digitization algorithm from SIM hits
def CaloWaveformDigiCfg(flags, name="CaloWaveformDigiAlg", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.WaveformDigitisationTool(name="CaloWaveformDigtisationTool", **kwargs)
    
    kwargs.setdefault("CaloHitContainerKey", "EcalHits")
    kwargs.setdefault("WaveformContainerKey", "CaloWaveforms")

    digiAlg = CompFactory.CaloWaveformDigiAlg(name, **kwargs)
    kwargs.setdefault("WaveformDigitisationTool", tool)
    
    digiAlg.CB_alpha = -0.9
    digiAlg.CB_n = 10
    digiAlg.CB_sigma = 4
    digiAlg.CB_mean = 820


    acc.addEventAlgo(digiAlg)

    return acc

def CaloWaveformDigitizationOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Digi"""
    acc = ComponentAccumulator()
    ItemList = [
        "RawWaveformContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "RDO", ItemList))
    # ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Don't know what this does
    return acc
