import random
from math import pi, sin, cos, sqrt, log
from ParticleGun.samplers import Sampler, mksampler
import ROOT

class RadialPosSampler(Sampler):
    """
    Sampler of Position 3-vectors, for modelling a beamspot, based on radius.
    """

    def __init__(self, r, x, y, z, t = 0):
        self.radius = r
        self.x = x
        self.y = y
        self.z = z
        self.t = t

    @property
    def z(self):
        "z position sampler"
        return self._z
    @z.setter
    def z(self, z):
        self._z = mksampler(z)

    @property
    def t(self):
        "Time sampler"
        return self._t
    @t.setter
    def t(self, t):
        self._t = mksampler(t)

    @property
    def r(self):
        "r position sampler"
        fwhm = 2*self.radius
        sig = fwhm/(2 * sqrt(2 * log(2)))

        # return random.uniform(0, self.radius)
        return random.gauss(0, self.radius)
        # return random.gauss(0, sig)

    @property 
    def phi(self):
        "phi position sampler"
        return random.uniform(0, 2*pi)

    def shoot(self):
        r = self.r
        phi = self.phi
        x = self.x + r * cos(phi)
        y = self.y + r * sin(phi)
        z = self.z()
        t = self.t()

        return ROOT.TLorentzVector(x, y, z, t)

if __name__ == "__main__":
# test when run from command line

    import numpy as np
    import matplotlib.pyplot as plt

    xlist = []
    ylist = []

    r = RadialPosSampler(r = 1, x = 10, y = -10, z = 10)
    for i in range (10000):
        res = r.shoot()
        xlist.append(res.X())
        ylist.append(res.Y())

    xarr = np.array(xlist)
    yarr = np.array(ylist)

    plt.figure(figsize = (5,5))
    plt.subplot(2,2,1)
    plt.scatter(xarr, yarr, marker = "*")
    plt.subplot(2,2,2)
    plt.hist(yarr)
    plt.subplot(2,2,3)
    plt.hist(xarr)
    plt.tight_layout()
    plt.show()
