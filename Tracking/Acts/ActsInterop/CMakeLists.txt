
# Declare the package name:
atlas_subdir( ActsInterop )

# External dependencies:

find_package(Acts COMPONENTS Core)

# Component(s) in the package:
atlas_add_library( ActsInteropLib
                   src/*.cxx
                   PUBLIC_HEADERS ActsInterop
                   LINK_LIBRARIES 
                   AthenaBaseComps
                   Identifier
                   FaserDetDescr
                   TrackerIdentifier 
                   TrackerReadoutGeometry
                   ActsCore)


