#ifndef COMBINATORIALKALMANFILTERALG_H
#define COMBINATORIALKALMANFILTERALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointContainer.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsKalmanFilter/TruthBasedInitialParameterTool.h"
#include "FaserActsKalmanFilter/SPSimpleInitialParameterTool.h"
#include "FaserActsKalmanFilter/SPSeedBasedInitialParameterTool.h"
#include "Acts/TrackFinding/CombinatorialKalmanFilter.hpp"
#include "Acts/TrackFinding/MeasurementSelector.hpp"
#include "FaserActsKalmanFilter/Measurement.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "FaserActsKalmanFilter/TrajectoryWriterTool.h"
#include "TrkTrack/TrackCollection.h"

class FaserSCT_ID;

namespace Trk
{
class TrackStateOnSurface;
}

namespace TrackerDD {
  class SCT_DetectorManager;
} 

class CombinatorialKalmanFilterAlg : public AthReentrantAlgorithm { 
 public:
  CombinatorialKalmanFilterAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~CombinatorialKalmanFilterAlg() = default;

  StatusCode initialize() override;
  StatusCode execute(const EventContext& ctx) const override;
  StatusCode finalize() override;

  using TrackFinderOptions =
      Acts::CombinatorialKalmanFilterOptions<IndexSourceLinkAccessor, MeasurementCalibrator, Acts::MeasurementSelector>;
      using FitterResult = Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>;
      using TrackFinderResult = std::vector<FitterResult>;
//  using TrackFinderResult = std::vector<
//      Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>>;
  using TrackFinderFunction = std::function<TrackFinderResult(
      const IndexSourceLinkContainer&, const std::vector<Acts::CurvilinearTrackParameters>&,
      const TrackFinderOptions&)>;

  static TrackFinderFunction makeTrackFinderFunction(
      std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry);

  Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;


 private:
  const FaserSCT_ID* m_idHelper{nullptr};
  const TrackerDD::SCT_DetectorManager* m_detManager{nullptr};

//  std::unique_ptr<Trk::Track> makeTrack(Acts::GeometryContext& tgContext, FitterResult& fitResult, std::vector<Tracker::FaserSCT_Cluster>  seed_spcollection) const;
  std::unique_ptr<Trk::Track> makeTrack(Acts::GeometryContext& tgContext, FitterResult& fitResult, std::vector<Tracker::FaserSCT_SpacePoint>  seed_spcollection) const;
  const Trk::TrackParameters* ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const;

  SG::WriteHandleKey<TrackCollection> m_trackCollection { this, "FaserActsCKFTrackCollection", "FaserActsCKFTrackCollection", "Output trackcollectionname" };

  ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool{this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
//  ToolHandle<SPSeedBasedInitialParameterTool> m_initialParameterTool{this, "InitialParameterTool", "SPSeedBasedInitialParameterTool"};
//  ToolHandle<SPSimpleInitialParameterTool> m_initialParameterTool{this, "InitialParameterTool", "SPSimpleInitialParameterTool"};
  ToolHandle<TruthBasedInitialParameterTool> m_initialParameterTool{this, "InitialParameterTool", "TruthBasedInitialParameterTool"};
  ToolHandle<TrajectoryWriterTool> m_trajectoryWriterTool{this, "OutputTool", "TrajectoryWriterTool"};
  SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
  SG::ReadHandleKey<FaserSCT_SpacePointContainer> m_SpacePointContainerKey{this, "SpacePointsSCTName", "SCT_SpacePointContainer", "SCT space point container"};
  SG::ReadHandleKey<Tracker::FaserSCT_ClusterContainer>  m_Sct_clcontainerKey{this, "SCT_ClusterContainer", "SCT_ClusterContainer"};
  mutable int m_nevents;
  mutable int m_ntracks;
  mutable int m_nseeds;
  mutable int m_nsp1;
  mutable int m_nsp2;
  mutable int m_nsp3;
  mutable int m_nsp10;
  mutable int m_nsp11;
  mutable int m_ncom;
};

#endif // COMBINATORIALKALMANFILTERALG_H
