################################################################################
# Package: FaserSpacePoints
################################################################################

# Declare the package name:
atlas_subdir( FaserSpacePoints )

atlas_add_component( FaserSpacePoints
                     src/components/*.cxx src/*.cxx src/*.h
	                   LINK_LIBRARIES AthenaBaseComps TrackerSpacePoint TrackerPrepRawData)

atlas_install_python_modules( python/*.py )