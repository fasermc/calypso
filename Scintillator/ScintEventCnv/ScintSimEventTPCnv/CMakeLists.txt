###############################################################################
# Package: ScintSimEventTPCnv
################################################################################

# Declare the package name:
atlas_subdir( ScintSimEventTPCnv )

# External dependencies:
find_package( CLHEP )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_library( ScintSimEventTPCnv
                   src/ScintHits/*.cxx
                   PUBLIC_HEADERS ScintSimEventTPCnv
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                   PRIVATE_DEFINITIONS ${CLHEP_DEFINITIONS}
                   LINK_LIBRARIES GaudiKernel GeneratorObjectsTPCnv ScintSimEvent AthenaPoolCnvSvcLib StoreGateLib SGtests
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} TestTools Identifier )

atlas_add_dictionary( ScintSimEventTPCnvDict
                      ScintSimEventTPCnv/ScintSimEventTPCnvDict.h
                      ScintSimEventTPCnv/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} AthenaPoolCnvSvcLib GaudiKernel GeneratorObjectsTPCnv ScintSimEvent TestTools StoreGateLib SGtests Identifier ScintSimEventTPCnv AthenaKernel )

