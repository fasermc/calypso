""" Define methods used to instantiate configured Waveform reconstruction tools and algorithms

Copyright (C) 2020 CERN for the benefit of the FASER collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

WaveformReconstructionTool = CompFactory.WaveformReconstructionTool
ClockReconstructionTool = CompFactory.ClockReconstructionTool

# One stop shopping for normal FASER data
def WaveformReconstructionCfg(flags, naive = False):
    """ Return all algorithms and tools for Waveform reconstruction """
    acc = ComponentAccumulator()

    if not flags.Input.isMC:
        acc.merge(WaveformClockRecCfg(flags, "ClockRecAlg"))

    if flags.Input.isMC  and naive:
        if "TB" not in flags.GeoModel.FaserVersion:
            acc.merge(PseudoScintHitToWaveformRecCfg(flags, "PseudoTimingHitWaveformRecAlg", "Trigger"))
        acc.merge(PseudoScintHitToWaveformRecCfg(flags, "PseudoVetoHitToWaveformRecAlg", "Veto"))
        acc.merge(PseudoScintHitToWaveformRecCfg(flags, "PseudoPresehowerHitWaveformRecAlg", "Preshower"))
        acc.merge(PseudoCaloHitToWaveformRecCfg(flags, "PseudoCaloHitWaveformRecAlg"))                
        return acc

    if "TB" not in flags.GeoModel.FaserVersion:
        acc.merge(WaveformHitRecCfg(flags, "TimingWaveformRecAlg", "Trigger"))
    acc.merge(WaveformHitRecCfg(flags, "VetoWaveformRecAlg", "Veto"))
    acc.merge(WaveformHitRecCfg(flags, "PreshowerWaveformRecAlg", "Preshower"))
    acc.merge(WaveformHitRecCfg(flags, "CaloWaveformRecAlg", "Calo"))

    return acc

# Return configured WaveformClock reconstruction algorithm
def WaveformClockRecCfg(flags, name="ClockRecAlg", **kwargs):

    acc = ComponentAccumulator()

    tool = ClockReconstructionTool(name="ClockReconstructionTool")
    # tool.CheckResult = True
    kwargs.setdefault("ClockReconstructionTool", tool)

    recoAlg = CompFactory.WaveClockRecAlg(name, **kwargs)
    recoAlg.ClockReconstructionTool = tool
    acc.addEventAlgo(recoAlg)

    return acc

# Return configured WaveformHit reconstruction algorithm
# Specify data source (Veto, Trigger, Preshower, Calo, Test)
def WaveformHitRecCfg(flags, name="WaveformRecAlg", source="", **kwargs):

    acc = ComponentAccumulator()

    if flags.Input.isMC:
        kwargs.setdefault("PeakThreshold", 5)

    tool = WaveformReconstructionTool(name=source+"WaveformRecTool", **kwargs)

    if "PeakThreshold" in kwargs: kwargs.pop("PeakThreshold")

    kwargs.setdefault("WaveformContainerKey", source+"Waveforms")
    kwargs.setdefault("WaveformHitContainerKey", source+"WaveformHits")
    kwargs.setdefault("WaveformReconstructionTool", tool)
              
    recoAlg = CompFactory.RawWaveformRecAlg(name, **kwargs)
    recoAlg.WaveformReconstructionTool = tool
    acc.addEventAlgo(recoAlg)

    return acc

def WaveformReconstructionOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Reco"""
    acc = ComponentAccumulator()
    ItemList = [
        "xAOD::WaveformHitContainer#*"
        , "xAOD::WaveformHitAuxContainer#*"
        , "xAOD::WaveformClock#*"
        , "xAOD::WaveformClockAuxInfo#*"
    ]
    acc.merge(OutputStreamCfg(flags, "xAOD", ItemList))
    # ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Don't know what this does
    return acc

# Return configured "reconstruction" algorithm from scint SIM hits
# Specify data source (Veto, Trigger, Preshower)
def PseudoScintHitToWaveformRecCfg(flags, name="PseudoScintHitToWaveformRecAlg", source="", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.PseudoSimToWaveformRecTool(name=source+"PseudoSimToWaveformRecTool", **kwargs)
    
    kwargs.setdefault("ScintHitContainerKey", source+"Hits")
    kwargs.setdefault("WaveformHitContainerKey", source+"WaveformHits")
    kwargs.setdefault("PseudoSimToWaveformRecTool", tool)

    
    recoAlg = CompFactory.PseudoScintHitToWaveformRecAlg(name, **kwargs)
    recoAlg.PseudoSimToWaveformRecTool = tool

    acc.addEventAlgo(recoAlg)

    return acc

# Return configured "reconstruction" algorithm from calo SIM hits
def PseudoCaloHitToWaveformRecCfg(flags, name="PseudoCaloHitToWaveformRecAlg", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.PseudoSimToWaveformRecTool(name="CaloPseudoSimToWaveformRecTool", **kwargs)
    
    kwargs.setdefault("CaloHitContainerKey", "EcalHits")
    kwargs.setdefault("WaveformHitContainerKey", "CaloWaveformHits")
    kwargs.setdefault("PseudoSimToWaveformRecTool", tool)
    
    recoAlg = CompFactory.PseudoCaloHitToWaveformRecAlg(name, **kwargs)
    recoAlg.PseudoSimToWaveformRecTool = tool

    acc.addEventAlgo(recoAlg)

    return acc
