// $Id: ObjectType.h 618658 2014-09-26 09:31:10Z krasznaa $
#ifndef XAODFASERBASE_OBJECTTYPE_H
#define XAODFASERBASE_OBJECTTYPE_H

// System include(s):
#include <iosfwd>

namespace xAOD {

   /// Namespace for the xAOD object types
   ///
   /// The reason for introducing an extra namespace like this is so the users
   /// will write things like
   /// <code>if( mypart->type() == xAOD::FaserType::Muon ) {...}</code>
   /// instead of using <code>xAOD::MuonType</code> or something similar.
   ///
   namespace FaserType {

      /// FaserType of objects that have a representation in the xAOD EDM
      ///
      /// xAOD classes identify themselves by all of them providing a function
      /// with the signature:
      ///
      ///  <code>
      ///  xAOD::FaserType::ObjectType type() const;
      ///  </code>
      ///
      /// This can be used to easily identify what sort of object some generic
      /// code is dealing with, avoiding doing a lot of
      /// <code>dynamic_cast</code>-s instead.
      ///
      /// Note that Doxygen doesn't allow to group enumeration variables
      /// together like it does for members of a class, that's why the grouping
      /// comments are not created according to the Doxygen rules.
      ///
      enum ObjectType {

         Other = 0, ///< An object not falling into any of the other categories

         // Reconstructed particle types
         // {

         CaloCluster  = 1, ///< The object is a calorimeter cluster

         Track   = 2, ///< The object is a charged track particle
         NeutralParticle = 3, ///< The object is a neutral particle

         Electron = 4, ///< The object is an electron
         Photon   = 5, ///< The object is a photon
         Muon     = 6, ///< The object is a muon

         // }

         // Reconstructed non-particle types
         // {

         Vertex = 101, ///< The object is a vertex

         // }

         // Truth types
         // {

         FaserTruthParticle    = 201, ///< The object is a truth particle
         FaserTruthVertex      = 202, ///< The object is a truth vertex
         FaserTruthEvent       = 203, ///< The object is a truth event
         FaserTruthPileupEvent = 204, ///< The object is a truth pileup event

         // }

         // Auxiliary types
         // {

         EventInfo   = 1001, ///< The object is an event information one
         EventFormat = 1002, ///< The object is an event format one

         Particle          = 1101, ///< Generic particle object, for analysis
         CompositeParticle = 1102  ///< Particle composed of other particles

         // }

      }; // enum ObjectType

   } // namespace FaserType

} // namespace xAOD

/// Convenience operator for printing the object type in a (debug) message
std::ostream& operator<< ( std::ostream& out, xAOD::FaserType::ObjectType type );

#endif // XAODFASERBASE_OBJECTTYPE_H
