################################################################################
# Package: Reconstruction
################################################################################

# Declare the package name:
atlas_subdir( Reconstruction )

# Component(s) in the package:
#atlas_add_component( GeoModelTest
#                     src/GeoModelTestAlg.cxx
#                     src/components/GeoModelTest_entries.cxx
#                     INCLUDE_DIRS ${GEOMODEL_INCLUDE_DIRS}
#                     LINK_LIBRARIES ${GEOMODEL_LIBRARIES} AthenaBaseComps GeoModelFaserUtilities ScintReadoutGeometry TrackerReadoutGeometry MagFieldInterfaces MagFieldElements MagFieldConditions )

# Install files from the package:
#atlas_install_joboptions( share/*.py )
#atlas_install_python_modules( python/*.py )
atlas_install_scripts( scripts/*.sh scripts/*.py )

atlas_add_test( ProdRecoTI12
    SCRIPT scripts/faser_reco.py ${CMAKE_CURRENT_SOURCE_DIR}/../rawdata/Faser-Physics-001920-filtered.raw TI12Data
    PROPERTIES TIMEOUT 300 )
atlas_add_test( ProdRecoTestBeam
    SCRIPT scripts/faser_reco.py ${CMAKE_CURRENT_SOURCE_DIR}/../RAWDATA/Faser-Physics-003613-filtered.raw TestBeamData
    PROPERTIES TIMEOUT 300 )

