#ifndef FASERSPACEPOINTS_H
#define FASERSPACEPOINTS_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointContainer.h"
#include <string>
#include <atomic>

#include "TTree.h"
#include "TFile.h"

namespace Tracker {
  class FaserSpacePoints : public AthReentrantAlgorithm {
   public:
    FaserSpacePoints(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~FaserSpacePoints() = default;

    virtual StatusCode initialize() override;
    virtual StatusCode execute(const EventContext& ctx) const override;
    virtual StatusCode finalize() override;

   private:
    void initializeTree();

    mutable int m_run{0};
    mutable int m_event{0};
    mutable int m_layer{0};
    mutable int m_nCollections{0};
    mutable int m_nSpacepoints{0};
    mutable float m_x{0};
    mutable float m_y{0};
    mutable float m_z{0};

    const FaserSCT_ID* m_idHelper{nullptr};
    SG::ReadHandleKey<FaserSCT_SpacePointContainer> m_sct_spcontainer{this, "SpacePointsSCTName", "SCT spacepoint container"};

    Gaudi::Property<std::string> m_filePath{this, "FilePath", "spacepoints.root", "Output root file for spacepoints"};
    Gaudi::Property<std::string> m_treeName{this, "TreeName", "tree", ""};

    TFile* m_outputFile;
    TTree* m_outputTree;
  };
}

#endif // FASERSPACEPOINTS_H
